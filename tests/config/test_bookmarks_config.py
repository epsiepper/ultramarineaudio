import pytest

from ultramarine_audio.config.bookmarks_config import BookmarkConfiguration, DOWNLOADED, ERROR, EXCLUDED, NOT_CONVERTED, UNAVAILABLE


def setup_bookmarksconfiguration():

    BookmarkConfiguration.save_location_path = None
    BookmarkConfiguration.config_file_name = "config.yaml"

    BookmarkConfiguration.downloaded_links = [
        "www.youtube.com/watch?v=abcdefgh",
        "www.youtube.com/watch?v=dQw4w9WgXcQ",
        "www.youtube.com/watch?v=aaaabbbb",
        "www.youtube.com/watch?v=123456789",
        "www.youtube.com/watch?v=ASDFGHJ",
        "www.youtube.com/watch?v=AqSwDeFr",
    ]
    BookmarkConfiguration.error_links = [
        "www.youtube.com/watch?v=00000000",
    ]
    BookmarkConfiguration.excluded_links = [
        "www.youtube.com/watch?v=99999999",
        "www.youtube.com/watch?v=Z",
    ]
    BookmarkConfiguration.not_converted_links = []
    BookmarkConfiguration.unavailable_links = [
        "www.youtube.com/watch?v=unavail",
        "www.youtube.com/watch?v=notthere",
        "www.youtube.com/watch?v=notthere2",
    ]

    BookmarkConfiguration._all_links = {
        DOWNLOADED: BookmarkConfiguration.downloaded_links,
        ERROR: BookmarkConfiguration.error_links,
        EXCLUDED: BookmarkConfiguration.excluded_links,
        UNAVAILABLE: BookmarkConfiguration.unavailable_links,
        NOT_CONVERTED: BookmarkConfiguration.not_converted_links,
    }

    assert 6 == len(BookmarkConfiguration.downloaded_links)
    assert 1 == len(BookmarkConfiguration.error_links)
    assert 2 == len(BookmarkConfiguration.excluded_links)
    assert 0 == len(BookmarkConfiguration.not_converted_links)
    assert 3 == len(BookmarkConfiguration.unavailable_links)


def test_bookmarksconfiguration_can_be_excluded():
    setup_bookmarksconfiguration()

    expected_results = [
        True,
        False,
        True,
        False,
        False,
        True,
        True,
        True,
        False,
    ]

    links_to_test = [
        "www.youtube.com/watch?v=99999999",
        "www.youtube.com/watch?v=555",
        "www.youtube.com/watch?v=dQw4w9WgXcQ",
        "www.youtube.com/watch?v=dQw4w9W",
        "www.youtube.com/watch?v=00000000",
        "www.youtube.com/watch?v=99999999",
        "www.youtube.com/watch?v=99999999",
        "www.youtube.com/watch?v=notthere",
        "www.youtube.com/watch?v=existing",
    ]

    for result, link in zip(expected_results, links_to_test):
        assert BookmarkConfiguration.can_be_excluded(link) is result


def test_bookmarksconfiguration_add_link():
    setup_bookmarksconfiguration()

    BookmarkConfiguration.add_link(EXCLUDED, "www.youtube.com/watch?v=abcd1234")
    BookmarkConfiguration.add_link(EXCLUDED, "www.youtube.com/watch?v=1234")
    BookmarkConfiguration.add_link(EXCLUDED, "www.youtube.com/watch?v=1234")
    BookmarkConfiguration.add_link(EXCLUDED, "www.youtube.com/watch?v=1234")
    BookmarkConfiguration.add_link(EXCLUDED, "www.youtube.com/watch?v=Z")

    BookmarkConfiguration.add_link(UNAVAILABLE, "www.youtube.com/watch?v=vvvvvv")

    BookmarkConfiguration.add_link(DOWNLOADED, "www.youtube.com/watch?v=1a2b3c4d5e")

    BookmarkConfiguration.add_link(NOT_CONVERTED, "www.youtube.com/watch?v=9876545")

    assert 7 == len(BookmarkConfiguration.downloaded_links)
    assert 1 == len(BookmarkConfiguration.error_links)
    assert 4 == len(BookmarkConfiguration.excluded_links)
    assert 1 == len(BookmarkConfiguration.not_converted_links)
    assert 4 == len(BookmarkConfiguration.unavailable_links)


def test_bookmarksconfiguration_remove_link():
    setup_bookmarksconfiguration()

    BookmarkConfiguration.remove_link(DOWNLOADED, "www.youtube.com/watch?v=abcdefgh")
    BookmarkConfiguration.remove_link(DOWNLOADED, "www.youtube.com/watch?v=ASDFGHJ")
    BookmarkConfiguration.remove_link(DOWNLOADED, "www.youtube.com/watch?v=ASDFGHJ")
    BookmarkConfiguration.remove_link(DOWNLOADED, "www.youtube.com/watch?v=ASDFGHJ")
    BookmarkConfiguration.remove_link(DOWNLOADED, "www.youtube.com/watch?v=ASDFGHJ")

    BookmarkConfiguration.remove_link(ERROR, "www.youtube.com/watch?v=00000000")

    BookmarkConfiguration.remove_link(NOT_CONVERTED, "www.youtube.com/watch?v=NONEXISTING")
    BookmarkConfiguration.remove_link(NOT_CONVERTED, "www.youtube.com/watch?v=NONEXISTING")
    BookmarkConfiguration.remove_link(NOT_CONVERTED, "www.youtube.com/watch?v=NONEXISTING")
    BookmarkConfiguration.remove_link(NOT_CONVERTED, "www.youtube.com/watch?v=NONEXISTINGAGAIN")

    BookmarkConfiguration.remove_link(UNAVAILABLE, "www.youtube.com/watch?v=unavail")

    assert 4 == len(BookmarkConfiguration.downloaded_links)
    assert 0 == len(BookmarkConfiguration.error_links)
    assert 2 == len(BookmarkConfiguration.excluded_links)
    assert 0 == len(BookmarkConfiguration.not_converted_links)
    assert 2 == len(BookmarkConfiguration.unavailable_links)


if __name__ == "__main__":
    pytest.main()
