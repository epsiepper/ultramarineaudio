import os

import pytest

from ultramarine_audio.download.bookmarks import process_bookmarks

bookmarks_test_base_parts = __file__.split(os.sep)[:-2]
bookmarks_test_base_parts.append("test_files")
bookmarks_test_base_path = os.sep.join(bookmarks_test_base_parts)


def test_process_bookmarks_with_firefox_json():
    path_to_json = os.path.join(bookmarks_test_base_path, "firefox_bookmarks.json")
    found_youtube_links = process_bookmarks(path_to_json)

    assert 7 == len(found_youtube_links)


def test_process_bookmarks_with_firefox_html():
    path_to_json = os.path.join(bookmarks_test_base_path, "firefox_bookmarks.html")
    found_youtube_links = process_bookmarks(path_to_json)

    assert 11 == len(found_youtube_links)


def test_process_bookmarks_with_chrome_html():
    path_to_json = os.path.join(bookmarks_test_base_path, "chrome_bookmarks.html")
    found_youtube_links = process_bookmarks(path_to_json)

    assert 15 == len(found_youtube_links)


def test_process_bookmarks_with_no_valid_bookmarks_path():
    found_youtube_links = process_bookmarks("totally/legit/bookmarks.json")

    assert found_youtube_links is None


if __name__ == "__main__":
    pytest.main()
