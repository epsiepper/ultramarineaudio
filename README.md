Contents
========

- [UltramarineAudio](#ultramarineaudio)
    - [About](#about)
    - [How to get](#how-to-get)
    - [How to use](#how-to-use)
- [Building](#building)
    - [From cloning to running](#from-cloning-to-running)
    - [Applying pytube fix](#applying-pytube-fix)
    - [Running the program](#running-the-program)
- [TODO](#todo)
- [FAQ](#faq)
    - [Installing dependencies](#installing-dependencies)
    - [Converting](#converting)
    - [Downloading](#downloading)

------------------------------------------------------------------------

UltramarineAudio
=========

About
-----

UltramarineAudio is a simple GUI for downloading and converting Youtube videos based on your Chrome or Firefox bookmarks.
It uses [pytube](https://python-pytube.readthedocs.io/en/latest/) for downloading, [pydub](https://github.com/jiaaro/pydub/) for converting, and the PyQt5 framework for the interface.

![](images/UltramarineAudio.png)

How to get
----------

A step by step guide from downloading the project to running the program can be found in the [Building](#building) section.

How to use
----------

UltramarineAudio requires your exported bookmarks in a .__html__ (Chrome, Firefox) or __.json__ (Firefox) file format. You also have to choose a 
directory, in which the downloaded and converted videos will be located.

Before downloading, you can select the videos, which shall be processed. Further, a configuration file will be
created in your prior chosen directory. This file will keep track of your already downloaded videos, your excluded videos,
and any videos, which could not be processed (e.g. the Youtube video is no longer available, so it could not be downloaded).
With this, future usage is more comfortable as it will do not any unnecessary processing of a video twice.


------------------------------------------------------------------------

Building
========

From cloning to running
-------

### Getting the sources

If you want to build the program yourself, instead of using one of the already built [binaries](#how-to-get), you first 
have to clone this project.

~~~
git clone https://gitlab.com/epsiepper/ultramarineaudio.git
~~~

### Installing dependencies

The entry point for the program is `ultramarine/main.py`. To be able to execute this, you have to install
the dependencies, which can be found in the `environment.yml`. 

To not pollute your basic Python installation, we will setup quickly a new virtual environment via `venv`. To do this 
we need the system library `python3-venv` installed

~~~
sudo apt-get install python3-venv
~~~

Now let us take care of the Python dependencies. Open a terminal and change directories into the cloned project. 

~~~
cd ultramarineaudio
~~~

There we do a 

~~~
python3 -m venv ./venv
~~~

to create a virtual environment called `venv`. We activate the virtual environment with

~~~
source venv/bin/activate
~~~

In the terminal you should see now a `(venv)` at the before your name. This indicates that the environment has been activated.

First, let us upgrade `pip` and `setuptools`, so there will not be any problems with installing our project dependencies

~~~
pip install --upgrade pip setuptools
~~~

With this, we can install the required dependencies, i.e.

~~~
pip install pydub pyqt5 pytube3 pyyaml 
~~~


The program should be runnable now, __but__ is not fully functional yet.


Applying pytube fix
-------------------

At the moment pytube (at least for the used version) has a problem that there is sometimes a key error when processing playlist videos.
This fails the download. and the next video will be processed.
  
To [fix this](https://github.com/nficano/pytube/issues/642#issuecomment-637671478), we have to modify a line in the downloaded pytube package.

Open the file

~~~
venv/lib/python3.7/site-packages/pytube/extract.py
~~~

and at the far bottom change the lines from
~~~
cipher_url = [
                parse_qs(formats[i]["cipher"]) for i, data in enumerate(formats)
            ]
~~~
to
~~~
cipher_url = [
                parse_qs(formats[i]["signatureCipher"]) for i, data in enumerate(formats)
            ]
~~~

Now, the program should be fully functional!


Running the program
-------------------

All dependencies have been installed and the fix has been applied, so let us start the program

~~~
python -m ultramarine_audio.main
~~~


------------------------------------------------------------------------

TODO
=====
Here are some features listed, which may be desirable, and will probably implemented next.

#### Optimization

- Decreasing wait time between downloading two videos without triggering any preventive measures

#### Quality settings

- Implementing settings to specify the quality of downloaded videos and converted files

------------------------------------------------------------------------

FAQ
=====

Installing dependencies
-----------------------

- I cannot install the `pyqt5` package.

    Try installing the system library `build-essential` to get the necessary compilers (i.e. g++). Then rerun `pip install pyqt5` in your 
    activated environment.

- `pytube` is installed, but when starting the program an `ImportError: cannot import name 'quote'` is thrown?

    There is difference between the `pytube` and the `pytube3` package. This error indicates that the `pytube` package is
    installed, however `pytube3` is needed. To fix this, open a terminal, activate your environment, then execute the command 
    `pip uninstall pytube`, and after this execute `pip install pytube3`. This should fix the problem. 
     

Converting
----------

- The first video will be downloaded and it can be found in the specified download directory, but then the program crashes. Why is that?

    This most likely happens because the program cannot find [FFmpeg](https://ffmpeg.org/). Internally, the dependency `pydub`
    uses the FFmpeg to convert mp4-files to mp3-files. Under Linux it can be installed via your preferred package manager. Under Windows
    it can be donwloaded from the official website, and it may be necessary to set the PATH environment variable.


Downloading
-----------

- The program is running and is trying to download videos one after another, but no video is downloaded actually?

    Most likely the [fix for pytube](#applying-pytube-fix) has not been applied.
