import sys

from PyQt5 import QtWidgets

from ultramarine_audio.ui.mainframe import MainWindow


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    main_window = MainWindow()
    main_window.setWindowTitle("UltramarineAudio")
    main_window.resize(800, 800)
    main_window.show()

    sys.exit(app.exec())
