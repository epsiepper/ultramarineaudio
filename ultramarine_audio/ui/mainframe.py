from json.decoder import JSONDecodeError
import os
import random
import sys
import time
from typing import Dict, List, Tuple

from PyQt5 import QtCore, QtGui, QtWidgets

from ultramarine_audio.config.bookmarks_config import BookmarkConfiguration, DOWNLOADED, ERROR, EXCLUDED, UNAVAILABLE, NOT_CONVERTED
from ultramarine_audio.config.settings_config import SettingsConfiguration
import ultramarine_audio.conversion.conversion as ua_conversion
import ultramarine_audio.download.bookmarks as ua_bookmarks
import ultramarine_audio.download.downloading as ua_downloading
from ultramarine_audio.ui.reset import ResetWidget
from ultramarine_audio.ui.settings import SettingsMenu

COLUMNS = ("Downloading", "Title")
PROJECT_URL = "https://gitlab.com/epsiepper/ultramarineaudio"

BACKGROUND_GREEN = QtGui.QColor(240, 255, 240)
BACKGROUND_GREY = QtGui.QColor(230, 230, 230)
BACKGROUND_RED = QtGui.QColor(255, 240, 240)
TEXT_BLACK = QtGui.QColor(0, 0, 0)
TEXT_GREEN = QtGui.QColor(0, 255, 0)
TEXT_RED = QtGui.QColor(255, 0, 0)


class ProcessingThread(QtCore.QThread):

    process_finished_signal = QtCore.pyqtSignal()
    process_log_signal = QtCore.pyqtSignal(str)
    process_progress_signal = QtCore.pyqtSignal(str, str)
    process_table_item_signal = QtCore.pyqtSignal(bool)

    def __init__(self, videos_to_download: List[Tuple[str, str]], target_dir: str):
        super().__init__()

        self.is_active = True

        self.videos_to_download = videos_to_download
        self._target_dir_base = target_dir
        self.target_dir_video = os.path.join(target_dir, "video")
        self.target_dir_music = os.path.join(target_dir, "music")

        if not os.path.exists(self.target_dir_music):
            os.makedirs(self.target_dir_music)

    def __del__(self):
        self.wait()

    def run(self):
        for url, title in self.videos_to_download:
            self.process_progress_signal.emit(title, url)

            # Random timeout to not flood youtube and to disguise bot action, because we are scraping
            for _ in range(random.randint(5, 12)):
                QtWidgets.QApplication.processEvents()
                if not self.is_active:
                    return
                time.sleep(1)

            try:
                self._process_link_to_mp3(url, title)
            except JSONDecodeError:
                self.process_log_signal.emit("The scraping prevention of Youtube has been activated. Wait for 10 seconds.")
                time.sleep(10)
                self._process_link_to_mp3(url, title)

        self.finished_processing()

    def _process_link_to_mp3(self, url: str, title: str):
        downloaded_video_path = ua_downloading.download_video(url, title, self.target_dir_video)

        if downloaded_video_path in (ERROR, UNAVAILABLE):
            BookmarkConfiguration.add_link(downloaded_video_path, url)
            self.process_log_signal.emit("Video could not be downloaded.")
            self.process_table_item_signal.emit(False)
            return

        BookmarkConfiguration.add_link(DOWNLOADED, url)

        if not SettingsConfiguration.shall_convert_videos:
            self.process_log_signal.emit("Video has been downloaded. Conversion will be skipped as configured.")
            self.process_table_item_signal.emit(True)
            return

        is_converted = ua_conversion.convert_mp4_to_mp3(downloaded_video_path, self.target_dir_music)

        if is_converted:
            self.process_log_signal.emit("Video has been converted.")
            self.process_table_item_signal.emit(True)
        else:
            BookmarkConfiguration.add_link(NOT_CONVERTED, url)
            self.process_log_signal.emit("Video could not be converted.")
            self.process_table_item_signal.emit(False)

        if not SettingsConfiguration.keep_downloaded_videos:
            self.process_log_signal.emit("Cleaning up downloaded video file.")
            os.remove(downloaded_video_path)

    def stop(self):
        self.is_active = False
        self.wait()

    def finished_processing(self):
        self.process_finished_signal.emit()


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()

        self.central_widget = CentralWidget()
        self.setCentralWidget(self.central_widget)

        self.reset_widget = ResetWidget()
        self.reset_widget.reset_update_signal.connect(self.central_widget.setup_table)
        self.settings_widget = SettingsMenu()

        self._setup_menubar()

    def _setup_menubar(self):

        menubar = self.menuBar()

        file_menu = menubar.addMenu("File")
        bookmark_action = QtWidgets.QAction("Open bookmark file (.json)", self)
        bookmark_action.triggered.connect(self.central_widget.on_bookmark_path_button)
        file_menu.addAction(bookmark_action)
        target_dir_action = QtWidgets.QAction("Set directory for downloaded videos", self)
        target_dir_action.triggered.connect(self.central_widget.on_target_dir_button)
        file_menu.addAction(target_dir_action)
        file_menu.addSeparator()
        reset_config_action = QtWidgets.QAction("Reset current saved configuration", self)
        reset_config_action.triggered.connect(self.reset_widget.show)
        file_menu.addAction(reset_config_action)
        file_menu.addSeparator()
        exit_action = QtWidgets.QAction("Exit", self)
        exit_action.triggered.connect(self.close)
        file_menu.addAction(exit_action)

        settings_menu = menubar.addMenu("Settings")
        settings_action = QtWidgets.QAction("Settings..", self)
        settings_action.triggered.connect(self.settings_widget.show)
        settings_menu.addAction(settings_action)

        about_menu = menubar.addMenu("About")
        project_action = QtWidgets.QAction("This project", self)
        project_action.triggered.connect(lambda: QtGui.QDesktopServices.openUrl(QtCore.QUrl(PROJECT_URL)))
        about_menu.addAction(project_action)

    def closeEvent(self, event):
        excluded_links = []

        for ix, url_and_title in self.central_widget.table_items.items():
            if ix not in self.central_widget.selected_videos_to_download:
                excluded_links.append(url_and_title[0])

        BookmarkConfiguration.excluded_links = excluded_links

        BookmarkConfiguration.write_to_file()
        QtWidgets.qApp.quit()


class CentralWidget(QtWidgets.QWidget):

    def __init__(self):
        super().__init__()

        self.table_items: Dict[int, Tuple[str, str]] = {}
        self.selected_videos_to_download: List[int] = []
        self.max_num_of_videos = len(self.selected_videos_to_download)
        self.processing_thread: ProcessingThread = None

        main_layout = QtWidgets.QVBoxLayout()

        input_layout = QtWidgets.QVBoxLayout()
        inner_input_layout = QtWidgets.QVBoxLayout()
        bookmark_inner_input_layout = QtWidgets.QHBoxLayout()
        self.bookmark_filepath_explanation = QtWidgets.QLabel("Chrome/Firefox bookmark file: ")
        bookmark_inner_input_layout.addWidget(self.bookmark_filepath_explanation)
        self.bookmark_filepath = QtWidgets.QLineEdit("")
        bookmark_inner_input_layout.addWidget(self.bookmark_filepath)
        self.bookmark_path_button = QtWidgets.QPushButton("..")
        self.bookmark_path_button.clicked.connect(self.on_bookmark_path_button)
        bookmark_inner_input_layout.addWidget(self.bookmark_path_button)
        target_dir_inner_input_layout = QtWidgets.QHBoxLayout()
        self.target_dir_path_explanation = QtWidgets.QLabel("Target folder for downloaded videos: ")
        target_dir_inner_input_layout.addWidget(self.target_dir_path_explanation)
        self.target_dir_path = QtWidgets.QLineEdit("")
        target_dir_inner_input_layout.addWidget(self.target_dir_path)
        self.target_dir_button = QtWidgets.QPushButton("..")
        self.target_dir_button.clicked.connect(self.on_target_dir_button)
        target_dir_inner_input_layout.addWidget(self.target_dir_button)
        inner_input_layout.addLayout(bookmark_inner_input_layout)
        inner_input_layout.addLayout(target_dir_inner_input_layout)
        input_layout.addLayout(inner_input_layout)
        main_layout.addLayout(input_layout)

        link_layout = QtWidgets.QVBoxLayout()
        select_all_layout = QtWidgets.QHBoxLayout()
        self.select_all_button = QtWidgets.QPushButton("Select all")
        self.select_all_button.hide()
        self.select_all_button.clicked.connect(self.on_select_all_button)
        self.deselect_all_button = QtWidgets.QPushButton("Deselect all")
        self.deselect_all_button.hide()
        self.deselect_all_button.clicked.connect(self.on_deselect_all_button)
        select_all_layout.addWidget(self.select_all_button)
        select_all_layout.addWidget(self.deselect_all_button)
        self.table: QtWidgets.QTableWidget = QtWidgets.QTableWidget(0, 0, self)
        link_layout.addLayout(select_all_layout)
        link_layout.addWidget(self.table)
        main_layout.addLayout(link_layout)

        download_layout = QtWidgets.QVBoxLayout()
        self.download_start_button = QtWidgets.QPushButton()
        self._update_download_button_text()
        self.download_start_button.clicked.connect(self.on_download_start_button)
        download_layout.addWidget(self.download_start_button)
        self.download_stop_button = QtWidgets.QPushButton("Stop downloads")
        self.download_stop_button.hide()
        self.download_stop_button.clicked.connect(self.on_download_stop_button)
        download_layout.addWidget(self.download_stop_button)
        self.progessbar = QtWidgets.QProgressBar()
        self.progessbar.setMaximum(0)
        self.progessbar.setValue(0)
        self.progessbar.hide()
        download_layout.addWidget(self.progessbar)
        main_layout.addLayout(download_layout)

        log_layout = QtWidgets.QVBoxLayout()
        self.logtext = QtWidgets.QTextEdit()
        self.logtext.setReadOnly(True)
        self.logtext.setMaximumHeight(100)
        log_layout.addWidget(self.logtext)
        main_layout.addLayout(log_layout)

        self.setLayout(main_layout)

    def setup_table(self):
        bookmarks: List[Tuple[str, str]] = ua_bookmarks.process_bookmarks(self.bookmark_filepath.text())

        if bookmarks is None:
            self.log("The bookmarks file at the given path does not exist.", is_error=True)
            return

        self.table_items: Dict[int, Tuple[str, str]] = {ix: val for ix, val in enumerate(bookmarks)}
        self.selected_videos_to_download = list(self.table_items.keys())

        self.table.setRowCount(len(self.table_items))
        self.table.setColumnCount(len(COLUMNS))
        self.table.setColumnWidth(0, 20)

        hheader = self.table.horizontalHeader()
        hheader.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)

        for ix, element in self.table_items.items():
            link_url = element[0]
            link_name = element[1]

            item_downloading = QtWidgets.QTableWidgetItem("")

            item_downloading.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled)
            item_downloading.setCheckState(QtCore.Qt.Checked)

            self.table.setItem(ix, 0, item_downloading)

            label = QtWidgets.QLabel('<a href="{}"> {}/</a>'.format(link_url, link_name))
            label.linkActivated.connect(lambda yt_url: QtGui.QDesktopServices.openUrl(QtCore.QUrl(yt_url)))
            self.table.setCellWidget(ix, 1, label)
            label.setAutoFillBackground(True)

        self.table.itemClicked.connect(self.handle_item_clicked)
        self._update_download_button_text()

    def set_color_for_table_row(self, row_index: int, background_color: QtGui.QColor):
        self.table.item(row_index, 0).setBackground(background_color)

        cell_widget = self.table.cellWidget(row_index, 1)
        cell_widget_palette = cell_widget.palette()
        cell_widget_palette.setColor(cell_widget.backgroundRole(), background_color)
        cell_widget.setPalette(cell_widget_palette)

    def _update_download_button_text(self):
        self.max_num_of_videos = len(self.selected_videos_to_download)
        self.download_start_button.setText("Start Download of {} videos".format(self.max_num_of_videos))

    def update_table(self):
        for ix, element in self.table_items.items():
            link_url = element[0]

            if BookmarkConfiguration.can_be_excluded(link_url):
                self.table.item(ix, 0).setCheckState(QtCore.Qt.Unchecked)
                if ix in self.selected_videos_to_download:
                    self.selected_videos_to_download.remove(ix)

        self._update_download_button_text()

    def on_bookmark_path_button(self):
        file_input = QtWidgets.QFileDialog.getOpenFileNames(self, 'Open file', '~', "Bookmark File (*.html *.json)")
        filepath = file_input[0][0]
        self.bookmark_filepath.setText(filepath)
        self.select_all_button.show()
        self.deselect_all_button.show()
        self.setup_table()

        if filepath.endswith(".html"):
            self.log("Read .html file '{}'".format(self.bookmark_filepath.text()))
        else:
            self.log("Read .json file '{}'".format(self.bookmark_filepath.text()))

        self.table.update()
        self.update()

    def on_target_dir_button(self):
        dir_path = QtWidgets.QFileDialog.getExistingDirectory(self, 'Open file', '~')
        self.target_dir_path.setText(dir_path)
        self.log("Videos will be downloaded to '{}'".format(dir_path))

        BookmarkConfiguration.read_from_file(dir_path)

        self.update_table()

    def on_select_all_button(self):
        for ix, element in self.table_items.items():
            video_link = element[0]
            self.table.item(ix, 0).setCheckState(QtCore.Qt.Checked)
            BookmarkConfiguration.remove_link(EXCLUDED, video_link)

        self.selected_videos_to_download = [ix for ix in range(0, len(self.table_items))]
        self._update_download_button_text()

    def on_deselect_all_button(self):
        for ix, element in self.table_items.items():
            video_link = element[0]
            self.table.item(ix, 0).setCheckState(QtCore.Qt.Unchecked)
            BookmarkConfiguration.add_link(EXCLUDED, video_link)

        self.selected_videos_to_download = []
        self._update_download_button_text()

    def on_download_start_button(self):
        if self.table.rowCount() == 0 or not self.selected_videos_to_download:
            self.log("No videos to download", is_error=True)
            return

        if not self.target_dir_path.text():
            self.log("Target directory, where videos should be saved, is not set", is_error=True)
            return

        self.download_start_button.hide()
        self.download_stop_button.show()

        self.progessbar.show()
        self.progessbar.setMaximum(self.max_num_of_videos)

        self.log("Starting to download..", is_success=True)

        videos_to_download: List[Tuple[str, str]] = []

        for ix in range(len(self.table_items)):
            if ix in self.selected_videos_to_download:
                videos_to_download.append(self.table_items[ix])
            else:
                self.set_color_for_table_row(ix, BACKGROUND_GREY)

        self.processing_thread = ProcessingThread(videos_to_download, self.target_dir_path.text())
        self.processing_thread.process_finished_signal.connect(self.on_download_stop_button)
        self.processing_thread.process_log_signal.connect(self.log)
        self.processing_thread.process_progress_signal.connect(self.update_progessbar)
        self.processing_thread.process_table_item_signal.connect(self.update_table_item_status)
        self.processing_thread.start()

    def on_download_stop_button(self):
        self.processing_thread.stop()

        self.update_table()

        self.progessbar.hide()
        self.progessbar.setValue(0)

        self.download_stop_button.hide()
        self.download_start_button.show()

    def update_progessbar(self, title: str, url: str):
        self.progessbar.setValue(self.progessbar.value() + 1)
        self.log("")
        self.log("{}/{}: {} - {}".format(self.progessbar.value(), self.max_num_of_videos, title, url))
        self.progessbar.setFormat("{} / {}".format(self.progessbar.value(), self.max_num_of_videos))

    def update_table_item_status(self, successfully_downloaded: bool):
        row_index = self.selected_videos_to_download.pop(0)

        if successfully_downloaded:
            self.set_color_for_table_row(row_index, BACKGROUND_GREEN)
        else:
            self.set_color_for_table_row(row_index, BACKGROUND_RED)

    def handle_item_clicked(self, item: QtWidgets.QTableWidgetItem):

        cur_row = item.row()
        video_link = self.table_items[cur_row][0]

        # Second expression in "if" and "elif" case is needed to prevent false counting, which can happen if the
        # item is clicked, but not inside the checkbox.
        if item.checkState() == QtCore.Qt.Checked and cur_row not in self.selected_videos_to_download:
            self.selected_videos_to_download.append(cur_row)
            BookmarkConfiguration.remove_link(EXCLUDED, video_link)
        elif item.checkState() == QtCore.Qt.Unchecked and cur_row in self.selected_videos_to_download:
            self.selected_videos_to_download.remove(cur_row)
            BookmarkConfiguration.add_link(EXCLUDED, video_link)

        self._update_download_button_text()

    def log(self, message: str, is_error: bool = False, is_success: bool = False):
        if is_error:
            self.logtext.setTextColor(TEXT_RED)
            self.logtext.append(message)
        elif is_success:
            self.logtext.setTextColor(TEXT_GREEN)
            self.logtext.append(message)
        else:
            self.logtext.append(message)

        self.logtext.setTextColor(TEXT_BLACK)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)

    main_window = MainWindow()
    main_window.setWindowTitle("UltramarineAudio")
    main_window.resize(800, 800)
    main_window.show()

    sys.exit(app.exec())
