from PyQt5 import QtWidgets

from ultramarine_audio.config.settings_config import SettingsConfiguration


class QHLine(QtWidgets.QFrame):
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)


class SettingsMenu(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("UltramarineAudio Settings")

        self._setup_layout()
        self.setup_configuration()

    def __call__(self):
        self.setup_configuration()

    def _setup_layout(self):
        settings_layout = QtWidgets.QVBoxLayout()

        default_button_layout = QtWidgets.QHBoxLayout()
        self.reset_default_button = QtWidgets.QPushButton("Reset to defaults")
        self.reset_default_button.clicked.connect(self.reset_configuration)
        default_button_layout.addWidget(self.reset_default_button)
        settings_layout.addLayout(default_button_layout)

        settings_layout.addWidget(QHLine())

        generic_settings_layout = QtWidgets.QVBoxLayout()
        self.shall_convert_videos_checkbox = QtWidgets.QCheckBox("Videos will be converted")
        self.shall_convert_videos_checkbox.setToolTip("If checked all downloaded videos will be converted to into the chosen audio format. If not checked no conversion will be done.")
        self.shall_convert_videos_checkbox.setChecked(True)
        generic_settings_layout.addWidget(self.shall_convert_videos_checkbox)
        self.keep_videos_checkbox = QtWidgets.QCheckBox("Keep downloaded videos")
        self.keep_videos_checkbox.setToolTip("If checked all downloaded videos will be kept. If not, then the videos will be deleted.")
        self.keep_videos_checkbox.setChecked(True)
        generic_settings_layout.addWidget(self.keep_videos_checkbox)
        settings_layout.addLayout(generic_settings_layout)

        # self.tabs = QtWidgets.QTabWidget()
        # self.tab_download = QtWidgets.QWidget()
        # self.tab_conversion = QtWidgets.QWidget()
        #
        # self.tabs.addTab(self.tab_download, "Download")
        # self.tabs.addTab(self.tab_conversion, "Conversion")
        #
        # settings_layout.addWidget(self.tabs)
        #
        # # Download tab
        # download_layout = QtWidgets.QVBoxLayout()
        #
        # timeout_layout = QtWidgets.QHBoxLayout()
        # self.timeout_download = QtWidgets.QLineEdit()
        # timeout_layout.addWidget(self.timeout_download)
        #
        # download_quality_layout = QtWidgets.QHBoxLayout()
        # self.download_quality_label = QtWidgets.QLabel("Quality: ")
        # self.download_quality = QtWidgets.QComboBox()
        # download_quality_layout.addWidget(self.download_quality_label)
        # download_quality_layout.addWidget(self.download_quality)
        #
        # download_layout.addLayout(timeout_layout)
        # download_layout.addLayout(download_quality_layout)
        #
        # self.tab_download.setLayout(download_layout)
        #
        # # Conversion tab
        # conversion_layout = QtWidgets.QVBoxLayout()
        #
        # conversion_quality_layout = QtWidgets.QHBoxLayout()
        # self.conversion_quality_label = QtWidgets.QLabel("Quality: ")
        # self.conversion_quality = QtWidgets.QComboBox()
        # conversion_quality_layout.addWidget(self.conversion_quality_label)
        # conversion_quality_layout.addWidget(self.conversion_quality)
        #
        # conversion_layout.addLayout(conversion_quality_layout)
        #
        # self.tab_conversion.setLayout(conversion_layout)

        # Confirmation buttons
        confirm_layout = QtWidgets.QHBoxLayout()
        self.save_button = QtWidgets.QPushButton("Save")
        self.save_button.clicked.connect(self.on_save_button)
        self.cancel_button = QtWidgets.QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.close)
        confirm_layout.addWidget(self.save_button)
        confirm_layout.addWidget(self.cancel_button)

        settings_layout.addLayout(confirm_layout)

        self.setLayout(settings_layout)

    def setup_configuration(self):
        saved_config = SettingsConfiguration.get_configuration()

        self.shall_convert_videos_checkbox.setChecked(saved_config["shall_convert_videos"])
        self.keep_videos_checkbox.setChecked(saved_config["keep_downloaded_videos"])

    def reset_configuration(self):
        self.shall_convert_videos_checkbox.setChecked(True)
        self.keep_videos_checkbox.setChecked(True)

    def on_save_button(self):
        new_config = {
            "shall_convert_videos": self.shall_convert_videos_checkbox.isChecked(),
            "keep_downloaded_videos": self.keep_videos_checkbox.isChecked(),
            "download_quality": "",
            "audio_file_format": "",
        }

        SettingsConfiguration.save_configuration(new_config)

        self.close()

    def show(self):
        self.setup_configuration()
        super().show()
