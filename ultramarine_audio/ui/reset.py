from PyQt5 import QtCore, QtWidgets

from ultramarine_audio.config.bookmarks_config import BookmarkConfiguration


class ResetWidget(QtWidgets.QDialog):

    reset_update_signal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()

        self._setup_layout()

    def _setup_layout(self):

        reset_layout = QtWidgets.QVBoxLayout()

        dialog_layout = QtWidgets.QLabel()
        dialog_layout.setText("Reset the status for all bookmarks.")
        reset_layout.addWidget(dialog_layout)

        confirmation_layout = QtWidgets.QHBoxLayout()
        self.accept_button = QtWidgets.QPushButton("Reset")
        self.accept_button.clicked.connect(self.accept)
        self.cancel_button = QtWidgets.QPushButton("Cancel")
        self.cancel_button.clicked.connect(self.close)
        confirmation_layout.addWidget(self.accept_button)
        confirmation_layout.addWidget(self.cancel_button)

        reset_layout.addLayout(confirmation_layout)

        self.setLayout(reset_layout)

    def accept(self):
        BookmarkConfiguration.reset_configuration()
        self.reset_update_signal.emit()
        self.close()
