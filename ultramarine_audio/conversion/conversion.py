from json.decoder import JSONDecodeError
import os
import subprocess

from pydub import AudioSegment


def convert_mp4_to_mp3(input_video: str, output_dir: str) -> bool:
    converted = False

    video_name = input_video.split(os.sep)[-1]
    title = video_name[:-4]
    output_mp3 = os.path.join(output_dir, title + ".mp3")

    try:
        mp4_version = AudioSegment.from_file(input_video, "mp4")
        mp4_version.export(output_mp3, format="mp3")

        converted = True

        return converted

    except (JSONDecodeError, MemoryError):
        print("Converting {} directly with ffmpeg".format(video_name))
        print("ffmpeg -i {} -vn {}".format(input_video, output_mp3))
        conversion_command = ["ffmpeg", "-i", input_video, "-vn", output_mp3]
        subprocess.run(conversion_command)

        if os.path.exists(output_mp3):
            converted = True

        return converted

    except Exception as e:
        print("Error occurred for video {}: {}".format(video_name, e))

        return converted
