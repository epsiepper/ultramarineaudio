from pytube import YouTube
from pytube.exceptions import RegexMatchError, VideoUnavailable

from ultramarine_audio.config.bookmarks_config import ERROR, UNAVAILABLE


def download_video(link_url: str, link_title: str, target_dir: str) -> str:
    try:
        audio_stream = YouTube(link_url).streams.filter(subtype='mp4').first()

        # Download the first video from the stream. Returns also the location where file is saved, so assign it to file_path
        file_path = audio_stream.download(target_dir)

        return file_path

    except OSError:
        # File name is too long
        file_path = audio_stream.download(target_dir, filename=link_title[:20])

        return file_path

    except (KeyError, RegexMatchError):
        return ERROR

    except VideoUnavailable:
        return UNAVAILABLE
