import json
import os
from typing import List, Optional, Tuple


def process_bookmarks(bookmarks_path: str) -> Optional[List[Tuple[str, str]]]:
    if os.path.isfile(bookmarks_path):

        if bookmarks_path.endswith(".html"):
            found_links_and_titles = _process_html_bookmarks(bookmarks_path)
        elif bookmarks_path.endswith(".json"):
            found_links_and_titles = _process_json_bookmarks(bookmarks_path)
        else:
            found_links_and_titles = None

        return found_links_and_titles

    else:
        return None


def _process_html_bookmarks(bookmarks_path: str) -> List[Tuple[str, str]]:
    """ Returns a list containing tuples of Youtube url and title for the given bookmark file.

    The html format is used by Chrome and Firefox.

    """
    link_title_data: List[Tuple[str, str]] = []

    with open(bookmarks_path, encoding="utf-8") as bk_file:
        for line in bk_file.readlines():
            if "www.youtube" not in line:
                continue

            temp = line.split('HREF="')[1]
            url = temp.split('" ADD_DATE')[0]

            temp = line.split("</A>")[0]
            title = temp.split(">")[-1]

            if title.lower().endswith("- youtube"):
                title = title[:-10]

            link_title_data.append((url, title))

    return link_title_data


def _process_json_bookmarks(bookmarks_path: str) -> List[Tuple[str, str]]:
    """ Returns a list containing tuples of Youtube url and title for the given bookmark file.

    The json format is used by Firefox.

    """
    with open(bookmarks_path, encoding="utf-8") as bk_file:
        bookmarks = json.load(bk_file)

    link_title_data: List[Tuple[str, str]] = []

    section_indices = [
        0,  # Bookmarks Menu
        1,  # Bookmarks Toolbar
        2,  # Other Bookmarks
        3,  # mobile
    ]

    for index in section_indices:
        try:
            bookmarks_of_section = bookmarks["children"][index]["children"]
        except KeyError:
            continue
        else:
            _search_youtubelinks_in_json(bookmarks_of_section, link_title_data)

    return link_title_data


def _search_youtubelinks_in_json(bookmarks: List[dict], link_information: List[Tuple[str, str]]):
    """ Recursive search for url links and related information for the link in the json bookmark file format
    of Firefox.

    Each bookmark has the following keys
    dict_keys(['guid', 'title', 'index', 'dateAdded', 'lastModified', 'id', 'annos', 'type', 'uri'])
    """
    for line in bookmarks:

        if "uri" in line:
            if "www.youtube" in line["uri"]:
                link_information.append((line["uri"], line["title"]))
        elif "children" in line:
            _search_youtubelinks_in_json(line["children"], link_information)
        else:
            continue
