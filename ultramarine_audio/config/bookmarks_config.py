from datetime import datetime
import os
from typing import Dict, List

import yaml

DOWNLOADED = "downloaded_links"
ERROR = "error_links"
EXCLUDED = "excluded_links"
UNAVAILABLE = "unavailable_links"
NOT_CONVERTED = "not_converted_links"


class BookmarkConfiguration:
    save_location_path: str = None

    # configuration file name to save and load configuration from
    config_file_name: str = "config.yaml"

    downloaded_links: List[str] = []  # links to videos which have already been downloaded
    error_links: List[str] = []  # links to videos which could not be processed because of youtube scraping prevention
    excluded_links: List[str] = []  # users self excluded links
    not_converted_links: List[str] = []  # links to videos which could not be converted, e.g. 1h videos
    unavailable_links: List[str] = []  # links to videos which could not be downloaded, e.g. video unavailable

    _all_links: Dict[str, List[str]] = {
        DOWNLOADED: downloaded_links,
        ERROR: error_links,
        EXCLUDED: excluded_links,
        UNAVAILABLE: unavailable_links,
        NOT_CONVERTED: not_converted_links,
    }

    @staticmethod
    def read_from_file(dir_path: str):
        BookmarkConfiguration.save_location_path = dir_path
        full_path = os.path.join(dir_path, BookmarkConfiguration.config_file_name)

        if not os.path.exists(full_path):
            return

        with open(full_path) as conf:
            loaded_all_links = yaml.load(conf, Loader=yaml.FullLoader)

        BookmarkConfiguration._all_links[DOWNLOADED] = loaded_all_links[DOWNLOADED]
        BookmarkConfiguration._all_links[ERROR] = loaded_all_links[ERROR]
        BookmarkConfiguration._all_links[EXCLUDED] = loaded_all_links[EXCLUDED]
        BookmarkConfiguration._all_links[NOT_CONVERTED] = loaded_all_links[NOT_CONVERTED]
        BookmarkConfiguration._all_links[UNAVAILABLE] = loaded_all_links[UNAVAILABLE]

    @staticmethod
    def write_to_file():
        if BookmarkConfiguration.save_location_path is None:
            return

        with open(os.path.join(BookmarkConfiguration.save_location_path, BookmarkConfiguration.config_file_name), "w") as conf:
            yaml.dump(BookmarkConfiguration._all_links, conf)

    @staticmethod
    def can_be_excluded(link: str) -> bool:
        for link_section in (BookmarkConfiguration._all_links[EXCLUDED],
                             BookmarkConfiguration._all_links[DOWNLOADED],
                             BookmarkConfiguration._all_links[UNAVAILABLE],
                             BookmarkConfiguration._all_links[NOT_CONVERTED]):
            if link in link_section:
                return True

        return False

    @staticmethod
    def add_link(section: str, link: str):
        if link not in BookmarkConfiguration._all_links[section]:
            BookmarkConfiguration._all_links[section].append(link)

    @staticmethod
    def remove_link(section: str, link: str):
        while link in BookmarkConfiguration._all_links[section]:
            BookmarkConfiguration._all_links[section].remove(link)

    @staticmethod
    def reset_configuration():
        if BookmarkConfiguration.save_location_path is None:
            return

        orig_config_name = BookmarkConfiguration.config_file_name
        orig_save_location = BookmarkConfiguration.save_location_path

        config_backup_location = os.path.join(BookmarkConfiguration.save_location_path, "config_backup")

        if not os.path.exists(config_backup_location):
            os.makedirs(config_backup_location)

        BookmarkConfiguration.config_file_name = str(datetime.now()) + BookmarkConfiguration.config_file_name
        BookmarkConfiguration.save_location_path = config_backup_location
        BookmarkConfiguration.write_to_file()

        BookmarkConfiguration.config_file_name = orig_config_name
        BookmarkConfiguration.save_location_path = orig_save_location

        BookmarkConfiguration.downloaded_links = []
        BookmarkConfiguration.error_links = []
        BookmarkConfiguration.excluded_links = []
        BookmarkConfiguration.not_converted_links = []
        BookmarkConfiguration.unavailable_links = []

        BookmarkConfiguration. _all_links = {
            DOWNLOADED: BookmarkConfiguration.downloaded_links,
            ERROR: BookmarkConfiguration.error_links,
            EXCLUDED: BookmarkConfiguration.excluded_links,
            UNAVAILABLE: BookmarkConfiguration.unavailable_links,
            NOT_CONVERTED: BookmarkConfiguration.not_converted_links,
        }

        BookmarkConfiguration.write_to_file()
        BookmarkConfiguration.read_from_file(BookmarkConfiguration.save_location_path)
