from typing import Any, Dict


class SettingsConfiguration:

    shall_convert_videos = True
    keep_downloaded_videos = True

    # Settings for youtube download such as 720p etc?
    download_quality = "Any"

    # Settings for mp3 conversion?
    audio_file_format = "mp3"

    @staticmethod
    def get_configuration() -> Dict[str, Any]:
        current_config = {
            "shall_convert_videos": SettingsConfiguration.shall_convert_videos,
            "keep_downloaded_videos": SettingsConfiguration.keep_downloaded_videos,
            "download_quality": SettingsConfiguration.download_quality,
            "audio_file_format": SettingsConfiguration.audio_file_format,
        }

        return current_config

    @staticmethod
    def save_configuration(new_config: Dict[str, Any]):

        SettingsConfiguration.shall_convert_videos = new_config["shall_convert_videos"]
        SettingsConfiguration.keep_downloaded_videos = new_config["keep_downloaded_videos"]
        SettingsConfiguration.download_quality = new_config["download_quality"]
        SettingsConfiguration.audio_file_format = new_config["audio_file_format"]

    @staticmethod
    def reset_to_default():
        SettingsConfiguration.shall_convert_videos = True
        SettingsConfiguration.keep_downloaded_videos = True
        SettingsConfiguration.download_quality = "Any"
        SettingsConfiguration.audio_file_format = "mp3"
