Changelog
=========

Version 1.2
-----------

- Added basic settings menu with options to not convert any videos or to remove the downloaded videos after conversion.
- Added colors to table rows while prcocessing to visualize the status if videos have been sucessfully downloaded and converted or not

Version 1.1
-----------

- Added Chrome bookmark support through processing bookmarks in .html file format

Version 1.0
-----------

- Finished basic program, which can read Firefox bookmarks in .json file format.